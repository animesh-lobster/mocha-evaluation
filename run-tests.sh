#!/bin/bash

# Gets the active branch name 
branch=$(git symbolic-ref --short -q HEAD)

# Set the test severity level according to the git branch
if [[ $branch == "master" ]]; then
    testLevel="critical"
    tag="critical"
elif [[ $branch == "develop" ]]; then
    testLevel="high"
    tag="critical|high"
else
    testLevel="low"
    tag="critical|high --invert"
fi

printf "———————————————————————————————————————————————\n"
printf "Test Setup\n"
printf "    • Git branch » %s\n" "$branch" 
printf "    • Test level » %s\n" "$testLevel"
printf "———————————————————————————————————————————————\n"

# Set environment variables
export TEST_LEVEL="$testLevel"

# Run tests
printf "\n» Running tests...\n"
npx mocha --grep @$tag