const http      = require("http")

const hostname  = "localhost"
const port      = 4000
const server    = http.createServer()

server.on("request", (_, response) => {
    response.setHeader("Content-Type", "application/json")
    response.status = 200
    response.end("Hello, Node!")
})

server.listen(port, hostname, () => {
    console.log(`Server listening at ${hostname}:${port}`)
})