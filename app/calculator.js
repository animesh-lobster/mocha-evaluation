function add(a, b) {
    return a + b;
}

function divide(a, b) {
    return a / b;
}

function squareRoot(a) {
    // if (a < 0) {
    //     a = Math.abs(a)
    //     return `${Math.sqrt(a)}i`;
    // }
    return Math.sqrt(a);
}

module.exports = {
    add,
    divide,
    squareRoot
}