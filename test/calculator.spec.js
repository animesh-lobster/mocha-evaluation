const Expect     = require("chai").expect
const Calculator = require("../app/calculator")

describe("◊◊  Calculator ◊◊", () => {
    // Addition test cases
    describe("> Addition", () => {
        it("Should add two positive numbers correctly", () => {
            const sum = Calculator.add(2, 3)
            Expect(sum).to.equal(5)
        })
    
        it("Should add a negative and positive number correctly", () => {
            const sum = Calculator.add(-4, 9)
            Expect(sum).to.equal(5)
        })

        it("Should add two negative numbers correctly", () => {
            const sum = Calculator.add(-3, -2)
            Expect(sum).to.equal(-5)
        })

        it("Should do integer arithmetic if a fractional value is given @high", () => {
            const sum = Calculator.add(3, 2.5)
            Expect(sum).to.equal(5)
        })

        it("Should throw an error if a non-number value is given @critical", () => {
            Expect(Calculator.add(3, "a")).to.throw
        })
    })

    // Test division
    describe("> Division", () => {
        it("Should always return positive infinity on division by 0", () => {
            const quotient = Calculator.divide(-11, 0)
            Expect(quotient).to.equal(Number.POSITIVE_INFINITY)
        })
    })

    // Test square root
    describe("> Square root @critical", () => {
        it("Should calculate square root of positive numbers", () => {
            const root = Calculator.squareRoot(2)   // 1.414
            Expect(root).to.be.approximately(1.414, 0.001)
        })

        it("Should return complex number (as a string) for negative input", () => {
            const root = Calculator.squareRoot(-16)
            Expect(root).to.equal("4i")
        })
    })
})